
let button = document.querySelector('.change-theme');



button.addEventListener('click', ()=> {
    document.body.classList.toggle('dark');
    let theme;
    

    if (document.body.classList.contains('dark')) {
        theme = 'DARK';
    }
    else {
        theme = 'LIGHT';
    }
    localStorage.setItem('PageTheme', theme);

})

let GetTheme = localStorage.getItem('PageTheme');

if(GetTheme === "DARK"){
    document.body.classList = 'dark';
}